<?php

namespace ApiChecker;

require_once 'vendor/autoload.php';
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use GuzzleHttp\Client;
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

class ApiChecker {
    public $auth;
    public $id;
    public $BASEURL = 'http://127.0.0.1:8000/api/';
    public $FILEDIR = 'C:/Users/aidan/Documents/git/ims/backend/';

    function __construct() {

        $env = getenv("BASEURL");

        print_r($env);

        $this->getCRUDFromControllers();
    }

    function getApiCalls() {
        $data = file_get_contents($this->FILEDIR . 'routes/api.php');
        
        $apis = new ArrayCollection();
        $controllers = new ArrayCollection();

        $apiCall = NULL;
        $controllerCall = NULL;

        for ($i = 0; $i < strlen($data); $i++) {

            //GETS API CALL AND STORES INTO APICALL
            if ($data[$i] == "c" && $data[$i + 1] == "e" && $data[$i + 2] == "(" && $data[$i + 3] == "'") {

                $x = $i + 4;
                $apiCall = '';

                while ($data[$x] != "'") {
                    $apiCall .= $data[$x];
                    $x++;
                }
            }

            //GETS API CONTROLLER AND STORES INTO CONTROLLERCALL
            if ($data[$i] == "o" && $data[$i + 1] == "l" && $data[$i + 2] == "l" && $data[$i + 3] == "e" && $data[$i + 4] == "r" && $data[$i + 5] == "s") {
                $x = $i + 7;
                $controllerCall = '';

                while ($data[$x] != "'") {
                    $controllerCall .= $data[$x];
                    $x++;
                }
            }

            //STORES API CALL AND API CONTROLLER INSIDE APIS COLLECTION
            if ($apiCall != NULL && $controllerCall != NULL) {
                $apis->set($apiCall, $controllerCall);
                $apiCall = NULL;
                $controllerCall = NULL;
            }
        }

        return $apis;
    }

    function getCRUDFromControllers () {
        $apis = $this->getApiCalls();

        $operations = new ArrayCollection();

        foreach ($apis as $call => $controller) {
            
            $requestMethods = array();

            $controllerFile = file_get_contents($this->FILEDIR . 'app/Http/Controllers/' . $controller . ".php");

            if (strpos(strtolower($controllerFile), 'index') !== false) {
                array_push($requestMethods, "GET");
            }

            if (strpos(strtolower($controllerFile), 'store') !== false) {
                array_push($requestMethods, "POST");
            }

            if (strpos(strtolower($controllerFile), 'show') !== false) {
                array_push($requestMethods, "SHOW");
            }

            if (strpos(strtolower($controllerFile), 'destroy') !== false) {
                array_push($requestMethods, "DELETE");
            }

            $operations->set($call, $this->api($requestMethods, $call));
        }

        print_r($operations);
    }

    function api ($method, $call) {
        $V = NULL;
    
        $headers = [
            'Authorization' => $this->auth,        
            'Accept'        => 'application/json',
        ];
    
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://127.0.0.1:8000/api/',
        ]);        
        
        $temp = array();

        //GETS THE METHOD OF THE API CALL
        if (in_array("GET", $method)) {
    
            $response = $client->request('GET', $call, [
                'headers' => $headers
            ]);

            array_push($temp, 'GET ' . $response->getStatusCode());

            $temp1 = json_decode($response->getBody());

            if (isset($temp1->data)) {
                $collection = new ArrayCollection($temp1->data) ;
            } else {
                $collection = new ArrayCollection($temp1);
            }

            $this->exportResponse($collection, $call, "get");

            $this->id = $collection->last()->id;
        } 
        
        if (in_array("SHOW", $method)) {
            $response = $client->request('GET', $call . "/" . $this->id, [
                'headers' => $headers
            ]);

            array_push($temp, 'SHOW ' . $response->getStatusCode());

            $temp1 = json_decode($response->getBody());
            $array =  (array) $temp1;
            $collection = new ArrayCollection($array);

            $this->exportResponse($collection, $call, "show");
        }

        if (in_array("DELETE", $method)) {
            $response = $client->request('DELETE', $call . "/" . $this->id, [
                'headers' => $headers
            ]);

            array_push($temp, 'DELETE ' . $response->getStatusCode());
        }

        return $temp;
    }

    function exportResponse ($response, $filename, $method) {

        $myfile = fopen("responses/${method}/${filename}.txt", "w") or die("Unable to open file!");

        if ($method == 'get') {
            foreach ($response as $key => $res) {
                $array = get_object_vars($res);
                $properties = array_keys($array);
    
                for ($index = 0; $index < sizeof($properties); $index++) {
    
                    $property = $properties[$index];
    
                    if ($res->$property == NULL) {
                        fwrite($myfile, $property . "  :  NULL\n");
                    } else {
                        fwrite($myfile, $property . "  :  " . $res->$property . "\n");
                    }
                }
    
                fwrite($myfile, "================================================================\n");
            }

        } else if ($method == 'show') {
            foreach ($response as $key => $res) {
                if ($res == NULL) {
                    fwrite($myfile, $key . "  :  NULL\n");
                } else {
                    fwrite($myfile, $key . "  :  " . $res . "\n");
                }
            }
        }

        fclose($myfile);
    }
}

$API = new ApiChecker;



?>
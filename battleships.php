<?php
class Battleships {

    public $userBoard = array (
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'),
        array('W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W')
    );

    public $userBoardBackend = array (
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
    );

    public $userInputAlready = array();
    public $userHit = 15;

    public function randomShipPlacement() {

        for ($ship = 5; $ship > 0; $ship--) {
            //GETS ANGLE
            $angle = rand(0, 1);

            //CREATES RANDOM X AND Y
            $x = rand(0, 9);
            $y = rand(0, 9);

            if ($angle == 1) {
     
                $down = sizeof($this->userBoard) - $x;
                $up = $x + 1;
                $valid = true;
      
                if ($ship <= $up) {

                    for ($index = 0; $index < $ship; $index++) {

                        $target = $this->userBoard[$index][$y];

                        if ($this->userBoard[$index][$y] == 'S') {

                            $ship = $ship + 1;
                            $valid = false;
                            break;
                        }
                    }

                    if ($valid) {
                        for ($index = 0; $index < $ship; $index++) {
                            $this->userBoard[$index][$y] = 'S';
                        }
                    }

                } else {

                    for ($index = $down - 1; $index > ($down - $ship) - 1; $index--) {

                        $target = $this->userBoard[$index][$y];

                        if ($this->userBoard[$index][$y] == 'S') {

                            $ship = $ship + 1;
                            $valid = false;
                            break;
                        }
                    }

                    if ($valid) {

                        for ($index = $down - 1; $index > ($down - $ship) - 1; $index--) {

                            $this->userBoard[$index][$y] = 'S';
                        }
                    }

                }

            } else {
    
                $right = sizeof($this->userBoard[$x]) - $y;
                $left = (10 - $right) + 1;
    
                //LEFT SIDE
                if ($ship <= $y) {

                    $leftSide = array_slice($this->userBoard[$x], 0, $left);
                    
                    //BASE CASE
                    if (in_array("S", $leftSide)) {

                        $ship = $ship + 1;
                        continue;
                    }

                    for ($i = $y; $i - $ship < $y; $i++) {

                        $this->userBoard[$x][$i  - $ship] = 'S';
                        
                    }

                //RIGHT SIDE
                } else {

                    $rightSide = array_slice($this->userBoard[$x], $y, $right);

                    //BASE CASE
                    if (in_array("S", $rightSide)) {

                        $ship = $ship + 1;
                        continue;
                    }

                    for ($i = $y; $i < $ship + $y; $i++) {
                        $this->userBoard[$x][$i] = 'S';
                    }
                }
            }
        }
    }

    public function status() {
        $this->randomShipPlacement($this->userBoard);
        $this->printBoard($this->userBoardBackend);

        while ($this->userHit != 0) {

            $coordinates = $this->userInput();
            
            $target = $this->userBoard[$coordinates[0]][$coordinates[1]];
            
            if ($target == 'S') {
                $this->userHit--;
                $this->userBoard[$coordinates[0]][$coordinates[1]] = 'H';
                $this->printBoard($this->userBoardBackend);

                echo "You hit a ship! \n";
                echo "Ships left: " . $this->userHit . "\n";
            } else {
                $this->printBoard($this->userBoardBackend);
                echo "You missed! \n";
            }
        }
    }
    
    public function printBoard($board) {
        $letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
        echo "   1   2   3   4   5   6   7   8   9   10  \n";

        for ($row = 0; $row < sizeof($board); $row++) {

            echo $letters[$row] . " ";

            for ($col = 0; $col < sizeof($board[$row]); $col++) {
              echo "[" . $board[$row][$col] . "] ";
            }
            echo "\n";
            echo "\n";
        }
    }

    public function userInput() {
        $coordinates = readline("Please input coordinates: ");

        $validValues = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10',
                        'b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9', 'b10',
                        'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10',
                        'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd8', 'd9', 'd10',
                        'e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', 'e10',
                        'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10',
                        'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'g7', 'g8', 'g9', 'g10',
                        'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8', 'h9', 'h10',
                        'i1', 'i2', 'i3', 'i4', 'i5', 'i6', 'i7', 'i8', 'i9', 'i10',
                        'j1', 'j2', 'j3', 'j4', 'j5', 'j6', 'j7', 'j8', 'j9', 'j10',
                       ];

        //CHECKS IF CORRDINATES IS WITHIN THE STRING
        if (strlen($coordinates) > 2 && 0 < strlen($coordinates)) {

            echo "User input is too long \n";
            $this->userInput();
        } else if (!(in_array(strtolower($coordinates), $validValues))) {

            echo "User input doesnt have valid coordinates \n";
            $this->userInput();
        } else if (in_array(strtolower($coordinates), $this->userInputAlready)) {

            echo "User has already used these coordinates \n";
            $this->userInput();
        }

        //PUSHES THIS TO ARRAY FOR ALREADY USED COORDINATES
        array_push($this->userInputAlready, strtolower($coordinates));

        //GET THE LETTER
        $letter = substr($coordinates, 0, 1);

        //USING ASSIC CHARACTERS TO GET THE VALUE OF THE LETTER
        $x = ord(strtolower($letter)) - 97;

        //GETTING THE VALUE FOR Y BY SUBSTACTING USER VALUE
        $y = substr($coordinates, 1) - 1;

        return [$x, $y];
    }
}

$battle = new Battleships;
$battle->status();

?>
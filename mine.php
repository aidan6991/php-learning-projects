<?php

require_once './vendor/autoload.php';

class Mine {

    public $mineAmount = 3;
    public $points = 0;

    public $boardBackend = array (
        array('S', 'S', 'S', 'S', 'S'),
        array('S', 'S', 'S', 'S', 'S'),
        array('S', 'S', 'S', 'S', 'S'),
        array('S', 'S', 'S', 'S', 'S'),
        array('S', 'S', 'S', 'S', 'S')
    );

    public $board = array (
        array('?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?')
    );

    function __construct() {
        $this->generateMines();
        $this->printBoard();
        $this->status();
    }

    function generateMines() {
        
        for ($index = 0; $index < $this->mineAmount; $index++) {
            
            $x = rand(0, 4);
            $y = rand(0, 4);
            
            if ($this->boardBackend[$x][$y] == 'M') {
                $this->mineAmount++;
            } else {
                $this->boardBackend[$x][$y] = 'M';
            }
        }
    }

    function status () {

        $userCoordinates = $this->userInput();

        if ($this->board[$userCoordinates[0]][$userCoordinates[1]] == "S") {

            echo "\n";
            echo "You have already checked this location!\n";
            $this->status();

        } else if ($this->boardBackend[$userCoordinates[0]][$userCoordinates[1]] == "S") {
        
            $this->board[$userCoordinates[0]][$userCoordinates[1]] = "S";
            $this->points += 100;

            echo "\n";
            $this->printBoard();
            echo "You have not hit a mine! 100 Points as been added!\n";
            echo "Total score is: " . $this->points ."\n";
            $this->status();
        
        } else {

            $this->board[$userCoordinates[0]][$userCoordinates[1]] = "M";
            $this->printBoard();

            echo "\n";
            echo "You have dead! Game Over\n";
            echo "Your total score: " . $this->points . "\n";
            return;
        }
    }

    function printBoard() {
        $letters = ["A", "B", "C", "D", "E"];
        echo "   1   2   3   4   5  \n";

        for ($row = 0; $row < sizeof($this->board); $row++) {

            echo $letters[$row] . " ";

            for ($col = 0; $col < sizeof($this->board[$row]); $col++) {
              echo "[" . $this->board[$row][$col] . "] ";
            }
            echo "\n";
            echo "\n";
          }
    }


    function userInput() {
        $coordinates = readline("Please input your coordinates for your guess: ");

        $validValues = ['a1', 'a2', 'a3', 'a4', 'a5',
                        'b1', 'b2', 'b3', 'b4', 'b5',
                        'c1', 'c2', 'c3', 'c4', 'c5',
                        'd1', 'd2', 'd3', 'd4', 'd5',
                        'e1', 'e2', 'e3', 'e4', 'e5',
                       ];

        //CHECKS IF CORRDINATES IS WITHIN THE STRING
        if (strlen($coordinates) > 2 && 0 < strlen($coordinates)) {
            echo "User input is too long \n";
            $this->userInput();
        } else if (!(in_array(strtolower($coordinates), $validValues))) {
            echo "User input doesnt have valid coordinates \n";
            $this->userInput();
        }

        //GET THE LETTER
        $letter = substr($coordinates, 0, 1);

        //USING ASSIC CHARACTERS TO GET THE VALUE OF THE LETTER
        $x = ord(strtolower($letter)) - 97;

        //GETTING THE VALUE FOR Y BY SUBSTACTING USER VALUE
        $y = substr($coordinates, 1) - 1;

        return [$x, $y];
    }
}

$Mine = new Mine();
?>
<?php
class SnakeLadders {

    public $userBoard = array (
        array('11', '12', '13', '14', '15', '16', '17', '18', '19', '20'),
        array('21', '22', '23', '24', '25', '26', '27', '28', '29', '30'),
        array('31', '32', '33', '34', '35', '36', '37', '38', '39', '40'),
        array('41', '42', '43', '44', '45', '46', '47', '48', '49', '50'),
        array('51', '52', '53', '54', '55', '56', '57', '58', '59', '60'),
        array('61', '62', '63', '64', '65', '66', '67', '68', '69', '70'),
        array('71', '72', '73', '74', '75', '76', '77', '78', '79', '80'),
        array('81', '82', '83', '84', '85', '86', '87', '88', '89', '90'),
        array('91', '92', '93', '94', '95', '96', '97', '98', '99', '100'),
        array('101', '102', '103', '104', '105', '106', '107', '108', '109', '110'),
    );

    public $userBoardBackend = array (
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
        array('?', '?', '?', '?', '?', '?', '?', '?', '?', '?'),
    );

    function welcome ($board) {

        global $y;
    
        echo "Hello its me" . $y;
    }

    function randomShipPlacement() {
        // 5 4 3 2
        

        for ($index = 5; $index > 1; $index--) {
            //GETS ANGLE
            $angle = 0; //rand(0, 1);

            if ($angle == 1) {
                $angle = "Horizontal";
                echo "1";
            } else {
                $x = rand(0, 9);
                $y = rand(0, 9);

                echo "X is " . $x . " and Y is " . $y . "\n";

                echo "This is the start point: " . $this->userBoard[$x][$y] . "\n";

                $right = sizeof($this->userBoard[$x]) - $y;
                echo "Right is " . $right . "\n";

                $left = (10 - $right) + 1;
                echo "Left is " . $left . "\n";

                echo "THE SHIP SIZE IS: " . $index . "\n";

                if ($index <= $y) {
    
                    echo "ITS LEFT HERE \n";

                    $leftSide = array_slice($this->userBoard[$x], 0, $left);
                    print_r($leftSide);

                    //BASE CASE
                    if (in_array("S", $leftSide)) {
                        $index = $index + 1;

                        echo "\n";
                        echo "SHIP FOUND ON LEFT \n";
                        echo "\n";

                        continue;
                    }

                    for ($i = $y; $i < $y + $index; $i++) {
                        $this->userBoard[$x][$i] = 'S';
                        echo "ADDED LEFT TO BOARD " . $i  . "\n";
                    }

                } else {

                    echo "ITS RIGHT HERE \n";

                    $rightSide = array_slice($this->userBoard[$x], $y, $right);
                    print_r($rightSide);
                    
                    //BASE CASE
                    if (in_array("S", $rightSide)) {
                        $index = $index + 1;

                        echo "\n";
                        echo "SHIP FOUND ON LEFT \n";
                        echo "\n";

                        continue;
                    }

                    for ($i = $y; $i < $index + $y; $i++) {
                        $this->userBoard[$x][$i] = 'S';
                        echo "ADDED TO BOARD ";
                    }
                }
                
                echo "\n";
                echo "WENT THROUGH LOOP \n";
                echo "\n";

            }



        }
    }

    function status() {

    }
    
    function printBoard($board) {
        $letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
        echo "   1   2   3   4   5   6   7   8   9   10  \n";

        for ($row = 0; $row < sizeof($board); $row++) {

            echo $letters[$row] . " ";

            for ($col = 0; $col < sizeof($board[$row]); $col++) {
              echo "[" . $board[$row][$col] . "] ";
            }
            echo "\n";
            echo "\n";
          }
    }

    function userInput() {
        $coordinates = readline("Please input coordinates: ");

        $validValues = ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10',
                        'b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9', 'b10',
                        'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10',
                        'd1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd8', 'd9', 'd10',
                        'e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', 'e10',
                        'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10',
                        'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'g7', 'g8', 'g9', 'g10',
                        'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8', 'h9', 'h10',
                        'i1', 'i2', 'i3', 'i4', 'i5', 'i6', 'i7', 'i8', 'i9', 'i10',
                        'j1', 'j2', 'j3', 'j4', 'j5', 'j6', 'j7', 'j8', 'j9', 'j10',
                       ];

        //CHECKS IF CORRDINATES IS WITHIN THE STRING
        if (strlen($coordinates) > 2 && 0 < strlen($coordinates)) {
            echo "User input is too long \n";
            $this->userInput();
        } else if (!(in_array(strtolower($coordinates), $validValues))) {
            echo "User input doesnt have valid coordinates \n";
            $this->userInput();
        }

        //GET THE LETTER
        $letter = substr($coordinates, 0, 1);

        //USING ASSIC CHARACTERS TO GET THE VALUE OF THE LETTER
        $x = ord(strtolower($letter)) - 97;

        //GETTING THE VALUE FOR Y BY SUBSTACTING USER VALUE
        $y = substr($coordinates, 1) - 1;

        return [$x, $y];
    }

    function test() {
        $this->randomShipPlacement($this->userBoard);
        $this->printBoard($this->userBoard);
    }
}


$battle = new Battleships;
$battle->test();

$original = array( 'a', 'b', 'c', 'd', 'e' );
$inserted = array( 'x' ); // not necessarily an array, see manual quote
 

print_r(array_splice($original, 3, 0, $inserted));
?>
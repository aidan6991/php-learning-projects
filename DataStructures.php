<?php
require_once 'vendor/autoload.php';
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

    class DataStructures {
        function collection() {
            $collection = new ArrayCollection([1, 2, 3]);
    
            $collection->then(function($value) {
                echo $value . "\n";
            });
        }

        function array() {
            $numbers = [1, 32, 32, 1, 3, 4];
            $numbers1 = [3, 1, 5, 5, 5];
            $a = [1, 2, 3, 4, 5];
            $b = array_map('cube', $a);
            print_r($b);
        }

        function stack () {
            
        }
        
        function queue () {

        }
    }

    $DataStructure = new DataStructures();

    $DataStructure->array();

?>
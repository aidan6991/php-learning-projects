<?php

require_once 'vendor/autoload.php';

use GuzzleHttp\Client;

$lives = 7;
$word = getRandomWord();
$found = array();

$hang[6] =
' -------
 |/    | 
 |
 |
 |
 |
 | 
/|\
';

$hang[5] =
' -------
 |/    | 
 |     o
 |
 |
 |
 | 
/|\
';


$hang[4] =
' -------
 |/    | 
 |     o
 |     |
 |     |
 |
 | 
/|\
';

$hang[3] =
' -------
 |/    | 
 |     o
 |     |
 |     |
 |    /
 | 
/|\
';

$hang[2] =
' -------
 |/    | 
 |     o
 |     |
 |     |
 |    / \
 | 
/|\
';

$hang[1] =
' -------
 |/    | 
 |     o
 |   --|
 |     |
 |    / \
 | 
/|\
';

$hang[0] =
' -------
 |/    | 
 |     o
 |   --|--
 |     |
 |    / \
 | 
/|\
';

/**
* returns a random word from API
*/

function getRandomWord() {

    $client = new Client();        
    $response = $client->request('GET', 'https://random-word-api.herokuapp.com/word?number=1&swear=0');

    $word = array();
    $word = json_decode($response->getBody());

    return $word[0];
}

/**
* starts the game and checks the status of the game
* 
* This function will first check the lives and then
* ask the user for a valid character. If the character
* is not valid it will recursive, asking the user.
* Once input has been again, it will check if the
* character is contained within the word. If the
* is contained it will print to the user that
* the input was found and show which postition
* the character is within the word. If the character
* isn't contained within word, then it will print
* that the input wasn't found and it will remove
* a life
*/
function game() {

    global $lives, $word, $found, $status, $hang;

    while ($lives > -1) {

        $status = 0;

        echo "====================================\n";

        //CHECKS IF THE LIVES HAS RAN OUT
        if ($lives == 0) {
            echo "Your are out of lives, you lose!\n";
            echo "The word was " . $word . "\n";
            return;
        }

        //PRINTS OUT THE CHARACTERS FOUND AND THE AMOUNT OF LETTERS
        for ($i = 0; $i < strlen($word); $i++) {
            if (in_array(strtolower($word[$i]), $found)) {
                echo $word[$i] . " ";
                $status++;
            } else {
                echo "_ ";
            }
        }

        echo "\n";

        //CHECKS IF THE USER HAS WON
        if ($status == strlen($word)) {
            echo "YOU WON!!!";
            return;
        }

        //TAKES USER INPUT
        $input = readline("Enter a character: ");

        //VALIDATE USER INPUT
        while (strlen($input) != 1) {
            $input = readline("Input invalid, please enter a character: ");
        }

        //APPENDS CHARACTERS FOR REGIX TO CHECK A RANGE AND FOR NO CASE SENSTIVE
        $check = "/" . $input . "/i";
        
        //CHECKS IF THE INPUT IS WITHIN THE WORD
        if (preg_match($check, $word)) {

            //CHECKS IF THE INPUT HAS ALREADY BEEN FOUND
            if (in_array(strtolower($input), $found)) {
                echo "You have already found this character!\n";
                continue;
            }

            //ADDS THE INPUT TO THE FOUND ARRAY
            array_push($found, strtolower($input));
            echo "The " .$input. " character was found within the word\n";
            continue;
        }


        //REMOVES A LIFE AND PRINTS THE HANGMAN DESIGN
        $lives--;
        echo "You have " . $lives . " amount of lives left\n";
        echo $hang[$lives];
    }
}

game();

?>